package pl.sda.singletable;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Data
@NoArgsConstructor
@Entity
@DiscriminatorValue("DIRECTOR")
public class DirectorV2 extends EmployeeV2 {

    private String department;

    public DirectorV2(String name, String surname, String department) {
        super(null, name, surname, null);
        this.department = department;
    }
}
