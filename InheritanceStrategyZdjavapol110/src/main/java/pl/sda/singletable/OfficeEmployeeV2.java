package pl.sda.singletable;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Data
@NoArgsConstructor
@Entity
@DiscriminatorValue("OFFICE_EMPLOYEE")
public class OfficeEmployeeV2 extends EmployeeV2 {

    private String skills;

    public OfficeEmployeeV2(String name, String surname, String skills) {
        super(null, name, surname, null);
        this.skills = skills;
    }

}
