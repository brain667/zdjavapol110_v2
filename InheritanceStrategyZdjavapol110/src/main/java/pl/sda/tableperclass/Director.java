package pl.sda.tableperclass;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "director_tableperclass")
public class Director extends Employee {

    private String department;

    public Director(String name, String surname, String department) {
        super(null, name, surname);
        this.department = department;
    }
}
