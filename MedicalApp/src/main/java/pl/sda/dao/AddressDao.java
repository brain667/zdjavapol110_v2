package pl.sda.dao;

import org.hibernate.Session;
import pl.sda.entity.Address;

public class AddressDao extends AbstractDao<Address> {

    public AddressDao(Session session, Class<Address> clazz) {
        super(session, clazz);
    }
}
