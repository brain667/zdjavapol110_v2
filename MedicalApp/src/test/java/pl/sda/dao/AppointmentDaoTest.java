package pl.sda.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pl.sda.entity.Appointment;
import pl.sda.entity.Patient;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class AppointmentDaoTest extends AbstractDaoTest {

    @Test
    public void shouldCancelAppointment() {

        //pobierz pacjenta po peselu i pobierz wszystkie przyszłe wizyty pacjenta
        //odwołaj najwczesniejsza wizyte z listy

        //given
        Session sessionPatient = sessionFactory.openSession();
        Session sessionAppointment = sessionFactory.openSession();
        Transaction transaction = sessionPatient.beginTransaction();
        PatientDao patientDao = new PatientDao(sessionPatient, Patient.class);
        AppointmentDao appointmentDao = new AppointmentDao(sessionAppointment, Appointment.class);

        final List<Appointment> futureAppointmentsByPesel = patientDao.getFutureAppointmentsByPesel(pesel);
        int appointmentsSize = futureAppointmentsByPesel.size();

        final Appointment earlierAppointment = futureAppointmentsByPesel
                .stream()
                .sorted(Comparator.comparing(Appointment::getDateFrom))
                .findFirst()
                .orElseGet(null);

        //when
        appointmentDao.cancel(earlierAppointment);
        transaction.commit();
        //then
        Assertions.assertEquals(appointmentsSize - 1, patientDao.getFutureAppointmentsByPesel(pesel).size());


        sessionPatient.close();

    }

}
