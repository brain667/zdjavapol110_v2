package pl.sda.onetoone;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String firstName;

    private String lastName;

    private LocalDate birthDate;

    //CASE SENSITIVE
    @OneToOne(mappedBy = "student") //relacja jest już zrobiona w StudentIndex na obiekcie o nazwie "student"
    private StudentIndex studentIndex;
}
