package pl.sda.compositekeys;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.math.BigDecimal;

public class CompositeKeysMain {

    public static void main(String[] args) {

        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        final Session session = sessionFactory.openSession();
        final Transaction transaction = session.beginTransaction();

        Account account = new Account("12345", "indywidaulne", new BigDecimal(1213.343));
        session.save(account);

        transaction.commit();
        session.close();
        sessionFactory.close();

    }
}
