package onetomany;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class OrderMain {

    public static void main(String[] args) {

        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        final Session session = sessionFactory.openSession();
        final Transaction transaction = session.beginTransaction();

        Client client = new Client(null, "mnowak");
        Order order1 = new Order(null, BigDecimal.valueOf(100.34), "woda", LocalDateTime.now(), client);
        Order order2 = new Order(null, BigDecimal.valueOf(14534.74), "gaz", LocalDateTime.now(), client);

        session.save(client);
        session.save(order1);
        session.save(order2);

        transaction.commit();
        session.close();
        sessionFactory.close();


    }

}
